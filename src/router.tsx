import { createBrowserRouter } from "react-router-dom";
import App from "./App";
import Ex1 from "./Ex1";
import Ex2 from "./Ex2";
import Ex3 from "./Ex3";


const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
  },
  {
    path: '/ex1',
    element: <Ex1 />,
  },
  {
    path: '/ex2',
    element: <Ex2 />,
  },
  {
    path: '/ex3',
    element: <Ex3 />,
  },
]);

export default router;