describe("useManagePeople", () => {
    it('initialises correctly', () => {
        // Initialise with default params, check you have an empty starting list
    })

    it('initialises with seeded list', () => {
        // Initialise with a seeded list, does that list come back out correctly?
    })

    it('adds a person correctly', () => {
        // Add a person, does that person appear in the list?

    });

    it('deletes a person correctly', () => {
        // Given a few people, delete a person, does that person disaappear from the list?

    })

})