import React from 'react';
import PersonForm from './PersonForm';
import useManagePeople from './useManagePeople';

const PersonManager: React.FC = () => {
    const { people, addPerson, deletePerson } = useManagePeople();

    const onDeletePerson: React.MouseEventHandler<HTMLButtonElement> = React.useCallback((e) => {
        deletePerson(e.currentTarget.id);
    }, [deletePerson])

    return (<div>
        <h2>People</h2>

        <PersonForm onSubmit={addPerson} />

        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {people.map(p => (<tr key={p.id}><td>{p.name}</td><td>{p.age}</td><td>
                    <button id={p.id} onClick={onDeletePerson}>Delete</button></td></tr>))}
            </tbody>
        </table>


    </div>)
}

export default PersonManager;