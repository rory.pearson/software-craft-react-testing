/**
 * Act's as a 'server' in that it holds a list of names which our fetchNames 'API' can return after a delay
 */

interface ServerState {
    names: string[];
    inError: boolean;
}

export const SERVER_STATE: ServerState = {
    names: ['alpha', 'bravo', 'charlie', 'delta', 'echo'],
    inError: false
}

export function setNames(names: string[]) {
    SERVER_STATE.names = names;
}
