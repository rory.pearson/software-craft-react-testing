import React from 'react';
import { v4 as uuid } from 'uuid';
import { SERVER_STATE } from './nameServer';

/**
 * This is just for purposes of demonstrating the 'Show Names' component, 
 * it modifies a global variable directly, 
 * this is not intended to show good practice, 
 * in fact...avert thy eyes
 * 
 * @returns Component
 */
const ModifyServer: React.FC = () => {

    // Just a bit of filth to change server state...don't mind me....
    const onToggleServerError = React.useCallback(() => SERVER_STATE.inError = !SERVER_STATE.inError, []);

    const onAddRandomUUID = React.useCallback(() => SERVER_STATE.names.push(uuid()), []);

    return <div>
        <h3>Modify Server Hacks</h3>
        <p>This component modifies the underlying 'server' data, just here for demo purposes.</p>

        <button role='button' onClick={onToggleServerError}>Toggle Server Error</button>
        <button role='button' onClick={onAddRandomUUID}>Add Random UUID as Name</button>

    </div>

}

export default ModifyServer;