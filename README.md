# Software Craft Guild - React Testing Library

Having presented on the React Testing Library, and some of the pitfalls.
This repo provides you with a chance to experiment with testing some simple components.

# How to run

This application is built with Vite, to run the application

`npm run dev`

To run all of the unit tests

`npm run test`

The goal is to write unit tests (filling in the cases that have been provided).

## Exercise 1

Write a test for the Counter in `Ex1/Counter.test.tsx`

## Exercise 2

### Part 1

Write a test for the custom hook for managing people

`Ex2/useManagePeople.test.ts`

### Part 2

Write a test for the Person Form

`Ex2/PersonForm.test.tsx`

## Exercise 3

Write a test for Show Names component

`Ex3/ShowNames.test.tsx`